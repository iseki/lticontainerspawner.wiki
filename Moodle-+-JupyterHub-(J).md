# Index

[[_TOC_]]

# 概要

- **Moodle(LMS)** から **LTI**を利用して **JupyterHub** にSSOする際に，幾つかの **LTIカスタムパラメータ**を渡し**JupyterHub を制御する**．
- さらに **JupyterHub** から**環境変数**を使用して，**コンテナ（Docker/Podman）を制御する**．
- これにより **LTI設定毎にJupyterHubを再起動させること無く，動的に設定（マウントするディレクトリや起動するJupyterコンテナイメージ）を変更することが可能となり，様々なJupyter環境を柔軟にユーザに提供することが可能となる．**

* Moodle側で LTIカスタムパラメータの設定補助を行うモジュールが [**mod_lticontainer**](https://gitlab.nsl.tuis.ac.jp/iseki/mod_lticontainer)
* JupyterHub側で LTIカスタムパラメータを処理し，コンテナの制御を行うのが [**LTIContainerSpawner**](https://gitlab.nsl.tuis.ac.jp/iseki/lticontainerspawner/)．
* コンテナ起動を制御するのが **start.sh** シェルスクリプト．


- 追加参照 Wiki
  - [**mod_lticontainer**](https://gitlab.nsl.tuis.ac.jp/iseki/mod_lticontainer/-/wikis/mod_lticontainer-(J))
  - [**LTIContainerSpawner**](./md/LTIContainerSpawner(J))

![](uploads/4e7947c00da7af25c11cb90d2e53c86d/LTICTR.png){width="65%"}
- 注) Moodle 4.1.5 よりコアから **xmlrpc機能が削除**されたので，現在は ltictr_proxyから Moodleへの通信は **REST**を使用している．

# 機能

- 以下の機能をコース内の外部ツール（LTI設定）毎に設定可能．（同じJupyterHubホストに対して複数同時設定が可能）
  - 教師ユーザと学生ユーザの分離．(LTIContainerSpawner)
  - ユーザ毎の柔軟な Volume のマウント，及びアクセス権（教師と学生）の設定．(mod_lticontainer + LTIContainerSpawner)
    - マウントする Volume を自由に指定可能．
    - 教材の配布の自動化と収集用 Volume の設定．
  - ユーザの作業用スペースの設定．(mod_lticontainer + LTIContainerSpawner)
    - ユーザの作業用スペースへ任意の課題を自動コピー可能．
  - マウントした Volume への任意名でのアクセス．(mod_lticontainer + LTIContainerSpawner)
  - 起動コンテナイメージのリモート選択．コンテナイメージ名の表示時にフィルタリングが可能．(mod_lticontainer + LTIContainerSpawner)
  - 起動 URL（Lab/Notebook）の選択．(mod_lticontainer + LTIContainerSpawner)
  - コンテナの使用する CPU/Memory リソースの制限．(LTIContainerSpawner)
- ユーザグループのサポート．(JupyterHub + LTIContainerSpawner)
- JupyterHub ホスト側にユーザのホームディレクトリが無い場合は，自動生成する．(JupyterHub + LTIContainerSpawner)
- コンテナとして Docker または Podman を使用可能．
- Volume のリモート生成と削除．(mod_lticontainer)
- iframe の一部サポート．(mod_lticontainer + LTIContainerSpawner)
  - 動くための条件がシビアなので（tornado のバージョンやWebブラウザの種類によって条件が変わる），"一部サポート" とする．


* オプション
  - ユーザの学習状況のリアルタイムでの確認と可視化．(ltictr_proxy + Moodle)

# 各システムの概要

## 前提となる既知システム（インストール手順の細部は省略）

### Moodle

- **v3.9.1** 以上
  - 3.9.0 以下でもLTIをサポートするパージョンであれば動く．
  - ただし v3.9.0 以下の場合は以下のコマンドで **instructorcustomparameters** カラムを **longtext** に変換する必要がある．
    - **`alter table mdl_lti modify column instructorcustomparameters longtext;`**
- Moodle ホストはJupyterHub が動くホストとは別のホストでもOK．
- JupyterHub で使用するユーザの認証が可能である必要がある（通常は**LDAP**などを用いる）．
- 外部ツール（LTI），Webサービス（~~XML-RPC~~ REST）（オプション）を使用する．
- Moodle ホスト側で，少なくとも **docker-ce-cli/docker-cli** または **podman-remote** がインストールされている必要がある．

### Container システム

- **Docker** または **Podman** が使用可能（両方を一台のホストにインストールして運用するのは不可能だと思われる．う～ん．頑張ればできるかもしれない....）

### JupyterHub

```plaintext
# pip3 install --upgrade pip
# npm install -g configurable-http-proxy
# pip install setuptools_rust
# pip install jupyterhub
# pip install dockerspawner
# pip install --upgrade notebook
# pip install --upgrade jupyterlab
```

- **DockerSpawner**
  - 通常は JupyterHub に付属してインストールされる．

### NSS（ユーザ情報）

- **JupyterHub ホスト側で，特にユーザ情報を同期する必要が無い場合は，この設定は行わなくても良い．**
  - ユーザID,グループID はLMS(Moodle) のIDを元に適当に割り振られる．グループは **others** になる．


* JupyterHub ホスト側でユーザ情報の同期を取る場合は，別途ユーザ情報（/etc/passwd, /etc/group形式）が必要（パスワード自体の情報は不要）．
  * **getent passwd** コマンドでユーザの情報（/etc/passwd形式）が取れることが必要．（および /etc/group形式の情報も）
* ユーザ情報が元々ローカルに無い場合は，Moodle ホストと同様に **LDAP** を用いても良いが，**altfiles** を用いても良い．

#### altfiles

- /etc/passwd, /etc/group 以外のファイルからユーザ情報を得るNSS用モジュール．
- ファイルの設置場所はコンパイル時に指定

```plaintext
# git clone https://github.com/aperezdc/nss-altfiles.git
# cd nss-altfiles/
# ./configure --prefix=/usr --datadir=/usr/local/etc --with-type=pwd,grp
# make
# make install 
# ln -s /usr/lib/libnss_altfiles.so.2 /usr/lib64/libnss_altfiles.so
# ldconfig
```

- この場合，/usr/local/etc/ に必要な passwd, group ファイルをコピーする．
  - passwd (/etc/passwd), group (/etc/group) ファイルは機密扱いではないので，必要な場合はリモートホストから簡単にコピーして持ってこれる．
  - group ファイルはあまり変化しない筈であるし，エントリも少ないと思うので，/etc/group と統合しても良いかも知れない．
- **/etc/nsswitch.conf** ファイルで，passwd とgroup のエントリに対して，files の後ろに **altfiles** を追加する．

```plaintext:/etc/nsswitch.conf
passwd: files altfiles
group:  files altfiles
```

### LTI Authenticator

- Moodle から JupyterHub にSSOするための JupyterHub認証モジュール（Moodle側からは外部サービスを使用して接続する）
  - SSO時に様々なデータ（LTIカスタムパラメータ等）を JupyterHub側で受け取れる．
- v1.0.0 では Moodleで使用する場合はパッチが必要だったが，v1.2.0 (2021/9/2) ではパッチが不必要になった．
- **LTI1.1** を使用する．（LTI1.3は未検証）
- **oauthlib** が先にインストールされていなければならない．

```plaintext
# pip install oauthlib
# pip install jupyterhub-ltiauthenticator
```

- Moodle の外部ツールと JupyterHubの設定ファイル（jupyterhub_config.py）で，コンシューマ鍵と共有秘密鍵を合わせる必要がある．
  - 鍵は **openssl rand -hex 32** コマンドなどで生成する．

```python:jupyterhub_config.py
# 設定サンプル
# for LTI v1.5.0
c.JupyterHub.authenticator_class = 'ltiauthenticator.LTIAuthenticator'
c.LTI11Authenticator.consumers = {
   "b18e82ec683724743236fade71350720029a29144a585c66f6741d8e9c6e0d83" : "c0fe2924dbb0f4701d898d36aaf9fd89c7a3ed3a7db6f0003d0e825a7eccb41c"
}
c.LTI11Authenticator.username_key = 'ext_user_username'
```

### Culler（オプション）

- 接続の切れた docker/podman コンテナを削除する機能．
- 様々な種類がある．今回のシステムについては以下の物をお勧めする．

#### cull_idle_servers.py

- https://github.com/jupyterhub/jupyterhub/tree/a6b7e303df03865d6420f6bccdf627b39f1d0dc1/examples/cull-idle

```plaintext
# pip install pycurl
# wget https://raw.githubusercontent.com/jupyterhub/jupyterhub/a6b7e303df03865d6420f6bccdf627b39f1d0dc1/examples/cull-idle/cull_idle_servers.py
# cp cull_idle_servers.py /usr/local/bin
# chmod a+rx /usr/local/bin/cull_idle_servers.py
```

- JupyterHub の設定ファイル（jupyterhub_lticontainer_config.py）での設定（デフォルトで設定済）

```python:jupyterhub_config.py
import sys

c.JupyterHub.services = [
   {
       'name': 'idle-culler',
       'admin': True,
       'command': [
           sys.executable,
           '/usr/local/bin/cull_idle_servers.py',
           '--timeout=3600'
       ],
   }
]
```

---

## 我々の拡張（今回の追加機能）

- 詳細はリンク先を参照．
- MS Windows での使用は想定していない．検証も行っていない．
- 主に Linux + オンプレミス環境での使用を想定している．

### [**LTIContainerSpawner**](./md/LTIContainerSpawner(J)) （JupyterHub のSpawner モジュール）

- https://gitlab.nsl.tuis.ac.jp/iseki/lticontainerspawner

#### インストール

```plaintext
# git clone https://gitlab.nsl.tuis.ac.jp/iseki/lticontainerspawner.git
# cd lticontainerspawner
# make install 
```

* 配布ファイル
  * /usr/local/etc/jupyterhub_lticontainer_config.py （JupyterHub 設定ファイル）
  * /usr/local/etc/lticontainerspawner.py （LTIContainerSpawner 本体）
  * /usr/local/etc/lticontainerspawner_config.py （LTIContainerSpawner 設定ファイル．デフォルトで使用可能）
  * /usr/local/etc/ltictr_proxy.conf （Ltictr_Proxy 設定ファイル）
  * /usr/lib/systemd/system/jupyterhub.service （JupyterHub の起動ファイル）
  * /usr/lib/systemd/system/ltictr_proxy.service（Ltictr_Proxy の起動ファイル）
  * /usr/lib/systemd/system/podman.socket（Podman Socket の起動ファイル）
  * /usr/local/bin/ltictr_proxy_server（Ltictr_Proxy 本体）
  * /usr/local/bin/ltictr_api_server （Ltictr_Proxy 用 APIサーバ本体）

#### 設定

```plaintext
# vi /usr/local/etc/jupyterhub_lticontainer_config.py
   最低限 my_ip_addr, ltiauth_consumer_key, ltiauth_secret_key, 
         ssl_server_cert, ssl_private_key, api_token, time_zone, time_format を設定する． 
# vi /usr/local/etc/ltictr_proxy.conf
   最低限 LTICTR_Server_Cert と LTICTR_Private_Key を設定する．
# systemctl enable jupyterhub.service
```

#### 起動

```plaintext
# systemctl start  jupyterhub.service
```

## [mod_lticontainer](https://gitlab.nsl.tuis.ac.jp/iseki/mod_lticontainer/-/wikis/mod_lticontainer-(J)) (サポート用Moodle モジュール)

- https://gitlab.nsl.tuis.ac.jp/iseki/mod_lticontainer

```plaintext
# cd [Moodle Path]/mod
# git clone https://gitlab.nsl.tuis.ac.jp/iseki/mod_lticontainer.git
# mv mod_lticontainer lticontainer
# chown -R [wwwサーバの実効ユーザ].[wwwサーバの実効グループ] lticontainer
adminユーザで Moodleにログインする
```

## 使用可能なコンテナイメージ

一般の JupyterHub 用イメージも使用できるが，拡張機能は使用できない．

#### GPU無し

- www.nsl.tuis.ac.jp:5000/jupyterhub-ltictr/base-notebook (Python)
- www.nsl.tuis.ac.jp:5000/jupyterhub-ltictr/singleuser (Python)
- www.nsl.tuis.ac.jp:5000/jupyterhub-ltictr/scipy-notebook (Python+SciPy)
- www.nsl.tuis.ac.jp:5000/jupyterhub-ltictr/tensorflow-notebook (Python+Tensorflow)
- www.nsl.tuis.ac.jp:5000/jupyterhub-ltictr/datascience-notebook (Python, Julia, R)
- www.nsl.tuis.ac.jp:5000/jupyterhub-ltictr/vhtec-notebook (Python, C++, JS, PHP)
- www.nsl.tuis.ac.jp:5000/jupyterhub-ltictr/xeus-cling (Python, C, C++)
- www.nsl.tuis.ac.jp:5000/jupyterhub-ltictr/java-notebook (Python, Java, JS)
- www.nsl.tuis.ac.jp:5000/jupyterhub-ltictr/php-notebook (Python, PHP, JS)
- www.nsl.tuis.ac.jp:5000/jupyterhub-ltictr/swift-tensorflow (Python, Swift)
- www.nsl.tuis.ac.jp:5000/jupyterhub-ltictr/jupyterlab-broccoli (Blockly, XPython, JS, TS, Lua)

#### GPU有

- www.nsl.tuis.ac.jp:5000/jupyterhub-ltictr/base-notebook-gpu (Python)
- www.nsl.tuis.ac.jp:5000/jupyterhub-ltictr/scipy-notebook-gpu (Python+SciPy)
- www.nsl.tuis.ac.jp:5000/jupyterhub-ltictr/tensorflow-notebook-gpu (Python+Tensorflow)

#### ダウンロード例

```plaintext
# docker pull www.nsl.tuis.ac.jp:5000/jupyterhub-ltictr/singleuser    
```

## 拡張コマンド（各イメージで以下のコマンドが使用できる）

#### ユーザ用
- **submit**
  - Submit Volume が一つしかない場合，カレントドライブにあるファイルを自動的にSubmit Volumeにコピーする（ディレクトリはコピーされない）．
  - コピーするファイルを限定したい場合は，ファイル名（複数）を直接指定する．
  - コピーする際にユーザIDがファイル名の先頭に付加される．
  - 使い方：`!submit`， `!submit kadai.ipynb`

#### 教師用
- **extract**
  - Submit Volume が一つしかない場合，Submit Volume にある各ユーザのファイルから各設問ごとの採点用の ipynbファイルを生成する．
  - 使い方（コンソールから）：`!extract base_filename codenum [codenum] [codenum] ....`
  - 例：`!extract 1-1.ypynb 2 3 5`
- **tocsv**
  - 採点用の各設問ごとのipynbを各ユーザ毎にまとめたcsvファイルを生成する.
  - 使い方（コンソールから）：`!tocsv base_filename`
  - 例：`!tocsv 1-1.ipynb`

## [Ltictr_Proxy](md/ltictr_proxy(J)) (オプション)

- ユーザの学習状況のデータを収集する．
- [**mod_lticontainer**](https://gitlab.nsl.tuis.ac.jp/iseki/mod_lticontainer) で Charts機能を使用する場合に必要．
- JupyterHub の configurable-http-proxy の代わりとなるリバースプロキシ
- configurable-http-proxy との併用も可能．
- Moodle と JupyterHub に間に設置し，WebSocketのデータを解析して Moodle に~~XML-RPC~~ REST経由でデータを投げる．
  - Moodle は **Webサービス** を使用してこのデータを受け取り，DBに格納する．
  - Moodle から発行されたトークンを Ltictr_Proxyの設定ファイルに設定する必要がある．


* 起動例

```plaintext
# vi /usr/local/etc/ltictr_proxy.conf
# vi /usr/lib/systemd/system/ltictr_proxy.service
# systemctl enable ltictr_proxy.service
# systemctl start  ltictr_proxy.service
```

* **lticontainerspawner_config.py** で configurable-http-proxy が自動起動しないようにする．
```plaintext
proxy_should_start  = False                     # False means to use ltictr_proxy
proxy_cleanup       = True
proxy_api_url       = 'http://localhost:8001'
proxy_api_token     = "ABCDEFG"                 # Sltictr_proxy.conf のトークン設定(LTICTR_API_Token) と合わせる 
```

# トラブルシューティング

### あるユーザのアカウントが突然サーバエラー (500:Internal Server Error) で使用できなくなった．
* Moodleの lticontainerモジュール の **JupyterHub User** タブを選択し，該当ユーザを削除する．一般ユーザでも操作可能．

### Moodle 上で "The web server process does not have write access to its own home directory. Please check the permissions" のエラーメッセージが表示された．
* Moodle が動いているサーバで，自動更新などによりドキュメントルートの所有者が変更された．
* ドキュメントルートの所有者および所有グループをWWWサーバの実効ユーザおよび実行グループに変更する．
