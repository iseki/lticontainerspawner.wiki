# Index
[[_TOC_]]
# Overview
- When logging in from **Moodle(LMS)** to **JupyterHub** using **LTI**, passing some **LTI custom parameters** to control **JupyterHub**.
- In addition, use environment variables from **JupyterHub** to **control containers** of Docker/Podman.
-  This makes it possible to dynamically change the configuration (mounted directory or launched Jupyter container image) **without restarting the JupyterHub for each LTI configuration, and to provide users with various Jupyter environments flexibly**.

* The module that assists in setting LTI custom parameters on the Moodle side is [**mod_lticontainer**](https://gitlab.nsl.tuis.ac.jp/iseki/mod_lticontainer)
    * The name of **mod_ltids** has been changed to [**mod_lticontainer**](https://gitlab.nsl.tuis.ac.jp/iseki/mod_lticontainer) with the official release.
* [**LTIContainerSpawner**](https://gitlab.nsl.tuis.ac.jp/iseki/lticontainerspawner/) is to process LTI custom parameters on the JupyterHub side and to control the container
* The **start.sh** shell script controls the container startup.

- Additional Reference Wiki
    - [**mod_lticontainer**](https://gitlab.nsl.tuis.ac.jp/iseki/mod_lticontainer/-/wikis/mod_lticontainer)
    -  [**LTIContainerSpawner**](md/LTIContainerSpawner) 

<img src="uploads/4e7947c00da7af25c11cb90d2e53c86d/LTICTR.png" width="65%">

- Note: Since **the xmlrpc functionality was removed** from the core in Moodle 4.1.5, **REST** is now used for communication from ltictr_proxy to Moodle.

# Functions
- The following functions can be configured for each external tool (LTI setting) in the course. (Multiple simultaneous settings can be made for the same JupyterHub host.)
    - Separation of teachers and students.(LTIContainerSpawner)
    - Flexible mounting of volumes per user and setting of access permissions (teacher and student). (mod_lticontainer + LTIContainerSpawner)
        - The volume to be mounted can be freely specified.
        - Automate the distribution of teaching materials and set up volumes for collection.
    - Setting up the user's workspace. (mod_lticontainer + LTIContainerSpawner)
        - Automatically copy any teaching materials to the user's workspace.
    - Access mounted volumes by arbitrary name. (mod_lticontainer + LTIContainerSpawner)
    - Remote selection of container image. The container image name can be filtered when it is displayed. (mod_lticontainer + LTIContainerSpawner)
    - Select the launch URL (Lab/Notebook). (mod_lticontainer + LTIContainerSpawner)
    - Limit the CPU/Memory resources used by the container. (LTIContainerSpawner)
- Support for user groups. (JupyterHub + LTIContainerSpawner)
- If there is no user's home directory on the JupyterHub host, it will be created automatically. (JupyterHub + LTIContainerSpawner)
- Docker/Podman can be selected as a container system. (LTIContainerSpawner)
- Remote creation and deletion of volumes. (mod_lticontainer)
- Partial support for iframes. (mod_lticontainer + LTIContainerSpawner)
    - Since the conditions for it to work are very strict (conditions vary depending on the version of tornado and the type of web browser), it is "partially supported".
* Option
    - Real-time confirmation and visualization of users' learning status. (Ltictr_Proxy + Moodle)
 
# Overview of each system
## Prerequisite known systems (detailed installation instructions omitted)
### Moodle
- **v3.9.1** and later
  - If the version supports LTI, it will work with v3.9.0 or lower.
  - However, for v3.9.0 or lower, you need to convert **instructorcustomparameters** columns to **longtext** by the following command.
    - **`alter table mdl_lti modify column instructorcustomparameters longtext;`**
- The Moodle host can be a different host than the one where JupyterHub runs.
- Authentication of the user to be used on JupyterHub is required (usually using **LDAP**, etc.).
- Use external tools (LTI) and Web services (~~XML-RPC~~ REST) (optional). 
- At least **docker-ce-cli/docker-cli** or **podman-remote** must be installed on the Moodle host.

### Container System
- **Docker** or **Podman** can be used

### JupyterHub

```plaintext
# pip3 install --upgrade pip
# npm install -g configurable-http-proxy
# pip install setuptools_rust
# pip install jupyterhub
# pip install dockerspawner
# pip install --upgrade notebook
# pip install --upgrade jupyterlab
```
- **DockerSpawner**
    - It is usually installed with JupyterHub.

### NSS (User Information)
- **If you do not need to synchronize user information on the JupyterHub host, you do not need to do this setting.**
    - The user ID and group ID will be assigned based on the LMS (Moodle) ID. The group will be **others**.

* To synchronize user information on the JupyterHub host side, user information (/etc/passwd, /etc/group format) is required (password information is not required).
    * It is necessary to get user information (in /etc/passwd format) by **getent passwd** command. (and /etc/group information)
* If the user information is not local, you can use **LDAP** as with Moodle hosts, and you can also use **altfiles**.

#### altfiles 
- Module for NSS to obtain user information from files other than /etc/passwd and /etc/group.
- The location of the file is specified at compile time.

```plaintext
# git clone https://github.com/aperezdc/nss-altfiles.git
# cd nss-altfiles/
# ./configure --prefix=/usr --datadir=/usr/local/etc --with-type=pwd,grp
# make
# make install 
# ln -s /usr/lib/libnss_altfiles.so.2 /usr/lib64/libnss_altfiles.so
# ldconfig
```
- In this case, copy the necessary passwd and group files to /usr/local/etc/.
    - The passwd (/etc/passwd) and group (/etc/group) files are not classified, so they can be easily copied from the remote host if necessary.
    - The group file is not supposed to change much, and there are not many entries in it, so it may be better to merge it with /etc/group.
- In the **/etc/nsswitch.conf** file, add **altfiles** after files for the passwd and group entries.
```:/etc/nsswitch.conf
passwd: files altfiles
group:  files altfiles
```
### LTI Authenticator
- JupyterHub authentication module for SSO from Moodle to JupyterHub (Moodle will use an external service to connect)
    - Various data (LTI custom parameters, etc.) can be received at the JupyterHub side during SSO.
- v1.0.0 required a patch to work with Moodle, v1.2.0 (9/2/2021) does not require a patch.
- Use **LTI1.1**. (LTI1.3 has not been tested yet.)
- **oauthlib** must be installed first.
```
# pip install oauthlib
# pip install jupyterhub-ltiauthenticator
```
- You will need to match the consumer and shared secret keys in the Moodle external tools and the JupyterHub configuration file (jupyterhub_config.py).
    - The key can be generated by the **openssl rand -hex 32** command.
```
# Sample of settings
#  for LTI v1.5.0
c.JupyterHub.authenticator_class = 'ltiauthenticator.LTIAuthenticator'
c.LTI11Authenticator.consumers = {
   "b18e82ec683724743236fade71350720029a29144a585c66f6741d8e9c6e0d83" : "c0fe2924dbb0f4701d898d36aaf9fd89c7a3ed3a7db6f0003d0e825a7eccb41c"
}
c.LTI11Authenticator.username_key = 'ext_user_username'
```
### Culler (optional).
- Function to delete remobved docker/podman containers.
- There are many different types. For this system, we recommend the following.
#### cull_idle_servers.py
- https://github.com/jupyterhub/jupyterhub/tree/a6b7e303df03865d6420f6bccdf627b39f1d0dc1/examples/cull-idle
```plaintext
# pip install pycurl
# wget https://raw.githubusercontent.com/jupyterhub/jupyterhub/a6b7e303df03865d6420f6bccdf627b39f1d0dc1/examples/cull-idle/cull_idle_servers.py
# cp cull_idle_servers.py /usr/local/bin
# chmod a+rx /usr/local/bin/cull_idle_servers.py
```
-Configuration in the JupyterHub configuration file (jupyterhub_config.py)
(already set by default in LTIContainerSpawner)
```python:jupyterhub_config.py
import sys

c.JupyterHub.services = [
   {
       'name': 'idle-culler',
       'admin': True,
       'command': [
           sys.executable,
           '/usr/local/bin/cull_idle_servers.py',
           '--timeout=3600'
       ],
   }
]
```
----------------
## Our expansions (additional features)
- See the link for details.
- It is not intended to be used on MS Windows. It has not been tested.
- It is intended to be used mainly in a Linux + on-premise environment.

### [**LTIContainerSpawner**](md/LTIContainerSpawner) (Spawne module on JupyterHub)
- https://gitlab.nsl.tuis.ac.jp/iseki/lticontainerspawner

#### Install
```plaintext
# git clone https://gitlab.nsl.tuis.ac.jp/iseki/lticontainerspawner.git
# cd lticontainerspawner
# make install 
```

* Distributed files
  * /usr/local/etc/jupyterhub_lticontainer_config.py  (JupyterHub configuration file)
  * /usr/local/etc/lticontainerspawner.py 
  * /usr/local/etc/lticontainerspawner_config.py   (LTIContainerSpawner Configuration file. Available by default)
  * /usr/local/etc/ltictr_proxy.conf   (Ltictr_Proxy configuration file)
  * /usr/lib/systemd/system/jupyterhub.service   (JupyterHub startup files)
  * /usr/lib/systemd/system/ltictr_proxy.service  (Ltictr_Proxy startup files)
  * /usr/lib/systemd/system/podman.socket  (Podman Socket startup files)
  * /usr/local/bin/ltictr_proxy_server
  * /usr/local/bin/ltictr_api_server

#### Configurations
```plaintext
# vi /usr/local/etc/jupyterhub_lticontainer_config.py
   Set the minimum my_ip_addr, ltiauth_consumer_key, ltiauth_secret_key, 
                   ssl_server_cert, ssl_private_key, api_token, time_zone and time_format
# vi /usr/local/etc/ltictr_proxy.conf
   Set the minimum LTICTR_Server_Cert and LTICTR_Private_Key
# systemctl enable jupyterhub.service
```

#### Execution

```plaintext
# systemctl start  jupyterhub.service
```

## [mod_lticontainer](https://gitlab.nsl.tuis.ac.jp/iseki/mod_lticontainer/-/wikis/mod_lticontainer) (Moodle module)
- https://gitlab.nsl.tuis.ac.jp/iseki/mod_lticontainer

```
# cd [Moodle Path]/mod
# git clone https://gitlab.nsl.tuis.ac.jp/iseki/mod_lticontainer.git
# mv mod_lticontainer lticontainer
# chown -R [Effective user of the www server].[Effective group of the www server] lticontainer
Log in to Moodle as admin user
```

## Available container images
It is also possible to use a regular container image, but in that case, extensions will not be available.
#### no GPU
- www.nsl.tuis.ac.jp:5000/jupyterhub-ltictr/base-notebook (Python)
- www.nsl.tuis.ac.jp:5000/jupyterhub-ltictr/singleuser  (Python+etc.)
- www.nsl.tuis.ac.jp:5000/jupyterhub-ltictr/scipy-notebook  (Python+SciPy)
- www.nsl.tuis.ac.jp:5000/jupyterhub-ltictr/tensorflow-notebook (Python+Tensorflow)
- www.nsl.tuis.ac.jp:5000/jupyterhub-ltictr/datascience-notebook (Python, Julia, R)
- www.nsl.tuis.ac.jp:5000/jupyterhub-ltictr/vhtec-notebook (Python, C++, JS, PHP)
- www.nsl.tuis.ac.jp:5000/jupyterhub-ltictr/xeus-cling (Python, C, C++) 
- www.nsl.tuis.ac.jp:5000/jupyterhub-ltictr/java-notebook (Python, Java, JS)
- www.nsl.tuis.ac.jp:5000/jupyterhub-ltictr/php-notebookdokc (Python, PHP, JS)
- www.nsl.tuis.ac.jp:5000/jupyterhub-ltictr/swift-tensorflow (Python, Swift)
- www.nsl.tuis.ac.jp:5000/jupyterhub-ltictr/jupyterlab-broccoli (Blockly, XPython, JS, TS, Lua)

#### with GPU
- www.nsl.tuis.ac.jp:5000/jupyterhub-ltictr/base-notebook-gpu (Python)
- www.nsl.tuis.ac.jp:5000/jupyterhub-ltictr/scipy-notebook-gpu (Python+SciPy)
- www.nsl.tuis.ac.jp:5000/jupyterhub-ltictr/tensorflow-notebook-gpu (Python+Tensorflow)

#### Download example
```plaintext
# docker pull www.nsl.tuis.ac.jp:5000/jupyterhub-ltictr/singleuser   
```

## Extended commands (the following commands can be used in the above images).
#### For User (Student)
- **submit**
    - If there is only one Submit Volume, files on the current drive are automatically copied to the Submit Volume (directories are not copied).
    - If you want to copy only specific files, specify the file names directly.
    - The user ID is appended to the beginning of the file name when it is copied.
    - Usage: ``` !submit ```, ``` !submit kadai.ipynb```
#### For Teacher
- **extract**
    - If there is only one Submit Volume, ipynb files for scoring each question are generated from each user's file in the Submit Volume.
    - Usage (from the console): ```!extract base_filename codenum [codenum] [codenum] ....```
    - Example: ```!extract 1-1.ypynb 2 3 5```
- **tocsv**
    - Generate a csv file with ipynb for each question for scoring, one for each user.
    - Usage (from the console): ```!tocsv base_filename```
    - Example：```!tocsv 1-1.ipynb```
   
## [Ltictr_Proxy](md/ltictr_proxy) (optional).
- Collects the user's learning status data.
- Required when using Charts function with [**mod_lticontainer**](https://gitlab.nsl.tuis.ac.jp/iseki/mod_lticontainer).
- Reverse proxy that replaces configurable-http-proxy on JupyterHub.
- Can be used in conjunction with configurable-http-proxy.
- It sits between Moodle and JupyterHub, parses the WebSocket data, and throws the data to Moodle in the form of ~~XML-RPC~~ REST.
    - Moodle will then use **Web services** to receive this data and store it in a DB.
    - The token issued by Moodle will need to be set in the Ltictr_Proxy configuration file.

- Example of Execution
```plaintext
# vi /usr/local/etc/ltictr_proxy.conf
# vi /usr/lib/systemd/system/ltictr_proxy.service
# systemctl enable ltictr_proxy.service
# systemctl start  ltictr_proxy.service
```
* To prevent configurable-http-proxy from auto-starting on JupyterHub in **lticontainerspawner_config.py**.
```plaintext
proxy_should_startt = False
proxy_cleanup = False
proxy_api_url = 'http://localhost:8001'
proxy_api_token = "ABCDEFG"     # Match the token setting of ltictr_proxy (LTICTR_API_Token)
```

# Troubleshooting

### A user's account suddenly becomes unavailable due to a server error (**500:Internal Server Error**)
- Select the "**JupyterHub User**" tab of Moodle's lticontainer module and delete the user. The user can also be deleted by an ordinary user.

### "The web server process does not have write access to its own home directory. Please check the permissions" error message on the Moodle.
* The owner of the document root has been changed on the server running Moodle, e.g. by an automatic update.
* Change the owner and group of the document root to the effective user and execution group of the WWW server.
