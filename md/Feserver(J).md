### Feserver よりも [Ltictr_Proxy](md/Ltictr_Proxy(J)) を使用することをお勧めします．
# Index
[[_TOC_]]

# Feserver (feplg_nbws.soモジュール)
- subversionリポジトリ： svn co http://www.nsl.tuis.ac.jp/svn/linux/feserver/trunk feserver
## 概要
- 本来は MITM 的動作を行う試験用ツール（TCP中継プログラム）．
    - サーバとクライアントの間に入り，通信データを中継する．
- 各種モジュールを読み込むことにより，中継する通信データに対して内容の確認や色々な処理が可能．
#### モジュールの例
- feplg_nop.so モジュール
    - 何もしないで，通信データの中継のみを行うモジュール．
- feplg_nbws.so モジュール
    - Jupyter のWebSocket の通信を解析するモジュール．
## 機能
#### feplg_nbws.so モジュール
- Moodle と JupyterHub の中間に位置し，通信情報（主に WebSockt通信で**ユーザの学習状況に関するデータ**）を収集して XML-RPC で Moodle に渡すためのモジュール
- ユーザから見ると，リバースプロキシのように見える．
- 機能
    - 通信の中継
    - WebSocket 通信の解析．
        - **ユーザの学習状況に関するデータを収集**．
    - HTTP(S)通信の解析．
    - Cookie の挿入．
    - **収集したデータを Moodle へXML-RPC 通信で渡す**．（MoodleのWebサービスを使用）
- データの可視化等は開発中．

## インストールと起動
#### 既に LTIContainerSpawner をインストール済みの場合
```
# vi /usr/local/etc/nbws.conf
# vi /usr/lib/systemd/system/feserver.service
# systemctl enable feserver.service
# systemctl start  feserver.service
```

#### 手動でのインストールと起動
##### 手動インストール
- コンパイルには [JunkBox_Lib](https://www.nsl.tuis.ac.jp/xoops/modules/xpwiki/?JunkBox_Lib) が必要．
```
# svn co http://www.nsl.tuis.ac.jp/svn/linux/JunkBox_Lib/trunk JunkBox_Lib
# cd JunkBox_Lib
# ./config.sh
# ./configure --enable-ssl
# make
# cd ..
# svn co http://www.nsl.tuis.ac.jp/svn/linux/feserver/trunk feserver
# cd feserver
# make
# vi nbsw.conf
```

##### 手動起動
```
# ./fesvr -h 172.22.1.75:8000 -p 9000 -m feplg_nbws.so -c -s --conf nbsw.conf
```
- Usage... ./fesvr -h host_name[:port] -p port -m module_path [-s] [-c] [-i] [-u user] [-f pid_file] [-d]  [--conf config_file]  [--cert cert_file] [--key key_file]
    - **-h** : サーバFQDN + ポート番号．サーバのポート番号を省略した場合は，ローカルポートの番号と同じになる．必須
    - **-p** : ローカルポート番号．必須．
    - **-m** : 処理モジュール．必須．
    - **-u** : 実効ユーザ名．
    - **-i** : デーモンモードを無効化．スーパデーモン経由で使用する場合に指定する．
    - **-f** : pid ファイルを指定する．
    - **-d** : デバッグモード．喧しモード．
    - **-s** : サーバに対してSSL/TLS接続になる（fesrv はSSL/TLSクライアントとなる）．サーバ証明書の検証は行わない．
    - **-c** : クライアントに対してSSL/TLS接続になる（fesrv はSSL/TLSサーバとなる）．
    - **--conf** : 設定ファイル指定（**コマンドラインと共通する部分は，コマンドライン優先**）
    - **--cert** : -c を指定した場合のサーバ証明書：PEM形式．設定ファイル内でも指定可能．  デフォルトは /etc/pki/tls/certs/server.pem
    - **--key**  : -c を指定した場合のサーバの秘密鍵：PEM形式．設定ファイル内でも指定可能． デフォルトは /etc/pki/tls/private/key.pem

## 設定
### feplg_nbws.so の 設定ファイル（nbws.conf）
- ~~**Moodle_Host** = XML-RPC を行うホスト名（FQDNまたはIPアドレス）．~~
    - ~~デフォルトは localhost~~
    - LMS からの自動通知に変更 
- ~~**Moodle_Port** = XML-RPC を行うホストのポート番号．~~
    - ~~デフォルトは 80~~
    - LMS からの自動通知に変更 
- ~~**Moodle_TLS** = XML-RPC を行う際に HTTPS を使用するかどうか．~~
    - ~~デフォルトは FALSE．~~
    - LMS からの自動通知に変更 
    - 注:HTTPS通信を行う場合，相手（Moodleホスト）のサーバ証明書は検証しない．
- ~~**Moodle_Token** = Moodle から発行されたトークン．~~
    - ~~下記参照（必須）~~
    - LMS からの自動通知に変更
- ~~Moodle_URL~~ **XmlRpc_Path** = XML-RPC を行うURLのディレクトリ情報．
    - デフォルトは **/webservice/xmlrpc/server.php**
- ~~Moodle_DBAns~~ **XmlRpc_Response** = XML-RPC を行った場合の返答を表示するかどうか．
    - デバッグ用．デフォルトは **FALSE**
- ~~Moodle_Service~~ **XmlRpc_Service** = XML-RPC のサービス名．
    - デフォルトから変更する必要はない．変更すると動かなくなる．デバッグ用．
    - デフォルトは **mod_lticontainer_write_nbdata**
- ~~Moodle_HTTP~~ **XmlRpc_HTTPver** = XML-RPC を行う際の HTTPのプロトコルバージョン．
    - 1.1 か 1.0 を指定．多分 1.1 で問題ない．デフォルトは 1.1

##### モジュール共通の設定
- **Fesvr_Server_Cert** = Feserver 本体用の設定．HTTPS通信を行う場合のサーバ証明書（PEM形式）
    - デフォルトは /etc/pki/tls/certs/server.pem
- **Fesvr_Private_Key** = Feserver 本体用の設定．HTTPS通信を行う場合の秘密鍵ファイル（PEM形式）
    - デフォルトは /etc/pki/tls/private/key.pem

