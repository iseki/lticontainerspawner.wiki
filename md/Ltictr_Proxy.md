
# Index
[[_TOC_]]

## Overview
- Reverse proxy server to replace JupyterHub's [**configurable-http-proxy**](https://github.com/jupyterhub/configurable-http-proxy).
- Can be used with [**configurable-http-proxy**](https://github.com/jupyterhub/configurable-http-proxy).
- Required when using Charts function with [**mod_lticontainer**](https://gitlab.nsl.tuis.ac.jp/iseki/mod_lticontainer).
## Functions
- It consists of **ltictr_api_server** for controlling communication routes, which is notified by JupyterHub, and **ltictr_proxy_server** for actually relaying communication and analyzing WebSocket data.
- Normally, **ltictr_api_server** is invoked from **ltictr_proxy_server** as a child process.
    - **ltictr_proxy_server** and **ltictr_api_server** can be started independently.
- **ltictr_proxy_server** can also be used as an ordinary reverse proxy server for the web.
- Function Details
    - Supports JupyterHub's communication route control.
    - Relaying communication data.
    - Analysis of WebSocket data.
        - **Collects data on user's learning status**.
    - Analysis of HTTP(S) communication.
    - Inserting cookies. (to identify the course in Moodle)
    - **Passing the collected data to Moodle via REST**. (using Moodle web services)

## Command Arguments 
#### ltictr_proxy_server
```
Usage... %s -p client_side_port [-c] [-h host_url[:port]] [-a [api_url:]port] [-u user] [-d] 
                      [--noexecapi] [--chunked] [--conf config_file] 
                      [--cert cert_file] [--key key_file] [--pid pid_file]
```

- **-p** : Local server port number to receive Web browser request. Required.
- **-c** : SSL/TLS connection to the Web browser.
- **-h** : URL to explicitly specify the WWW server.
- **-a** : URL to specify ltictr_api_server. If only port number is used, the URL part is http://127.0.0.1
- **-u** : Effective user name.
- **-d** : Debug mode.
- **-n**, **--noexecapi** : Don't start ltictr_api_server.
- **--chunked** : Treats chunk data as chunk data. In normal operation, all chunk data is received first, and then transferred.
- **--conf** : Specify the configuration file (**conflicts with the command line, the configuration file has priority**).
- **--cert** : Server certificate file when -c is specified: PEM format. Can be specified in the configuration file.
    - Default is /etc/pki/tls/certs/server.pem.
- **--key** : The private key file of the server when -c is specified: PEM format. Can also be specified in the configuration file. 
    - Default is /etc/pki/tls/private/key.pem
- **--pid** : Specify the pid file.
    - Default is /var/run/ltictr_proxy.pid

#### ltictr_api_server
``` Usage...
Usage... %s -a [api_url:]port [-u user] [-d] 
               [--conf config_file] [--cert cert_file] [--key key_file] [--apid pid_file] 
```
- **-a** : URL and port number of the server port, required. Ignore the FQDN part. For SSL/TLS communication, https://:8001 is used.
- **-u** : Effective user name.
- **-d** : Debug mode.
- **--conf** : Specify the configuration file (**conflicts with the command line, the configuration file takes priority**).
- **--cert** : Server certificate file when -c is specified: PEM format. Can be specified in the configuration file.
    - Default is /etc/pki/tls/certs/server.pem.
- **--key** : The private key file of the server when -c is specified: PEM format. Can also be specified in the configuration file. 
    - Default is /etc/pki/tls/private/key.pem
- **--pid** : Specify the pid file.
    - Default is /var/run/ltictr_proxy.pid

## Execution
#### Using systemd
```
# vi /usr/local/etc/ltictr_proxy.conf
# vi /usr/lib/systemd/system/ltictr_proxy.service
# systemctl enable ltictr_proxy.service
# systemctl start ltictr_proxy.service
```

#### Manual Execution
##### 1. Normal Exec
```
# ltictr_proxy_server -p 8100 -c -a 8001 --conf /usr/local/etc/ltictr_proxy.conf
# jupyterhub -f jupyterhub_XXX_config.py
```
##### 2. If you want to start ltictr_api_server and ltictr_proxy_server separately
```
# ltictr_api_server -a 8001 --conf ./ltictr_proxy.conf
# ltictr_proxy_server -p 8100 -c -a 8001 -n --conf /usr/local/etc/ltictr_proxy.conf
# jupyterhub -f jupyterhub_XXX_config.py
``` 
##### 3. When used with configarable-http-proxy. Or when used as a regular web reverse proxy.
``` 
# ltictr_proxy_server -p 8100 -c -n -h http://202.26.150.55:8000 --conf /usr/local/etc/ltictr_proxy.conf
# jupyterhub -f jupyterhub_XXX_config.py
``` 

## Configuration
### JupyterHub configuration
- In **Manual Execution** above, in case of **1 or 2**, you have to prevent JupyterHub from starting configurable-http-proxy automatically.
- Configuration file of JupyterHub (jupyterhub_XXX_config.py)
```
proxy_should_start  = False                     # False means to use ltictr_proxy
#proxy_should_start  = True                      # True means to use configurable-http-proxy
proxy_cleanup       = True
proxy_api_url       = 'http://localhost:8001'
proxy_api_token     = "ABCDEFG"                 # Same value as LTICTR_API_Token in ltictr_proxy.conf
```

### Configuration file (/usr/local/etc/ltictr_proxy.conf)
- **Rest_Path** = Directory information for the URL to do the REST.
    - Default is **/webservice/rest/server.php**
- **Rest_Service** = The name of the REST service. 
    - There is no need to change from the default. If you change it, it will not work.
    - For debugging. Default is **mod_lticontainer_write_nblogs**
- **Rest_HTTPver** = HTTP protocol version for REST. 1.1 or 1.0
    - 1.1 or 1.0 Probably 1.1 is fine. The default is 1.1
- **Rest_Response** = Whether to print the response of REST.
    - For debugging. Default is **FALSE**.
- **LTICTR_PID_File** = pid file for ltictr_proxy_server.
- **LTICTR_API_TID_File** = pid file of ltictr_api_server.
- **LTICTR_API_Token** = connection token for the API server. 
    - It should be the same value as **c.ConfigurableHTTPProxy.auth_token** in the JupyterHub configuration file.
- **LTICTR_Server_Cert** = Server certificate (PEM format) for HTTPS communication.
    - Default is /etc/pki/tls/certs/server.pem
- **LTICTR_Private_Key** = Private key file (PEM format) for HTTPS communication.
    - Default is /etc/pki/tls/private/key.pem
