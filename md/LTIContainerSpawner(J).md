# Index

[[_TOC_]]

# 概要

- [**LTIContainerSpawner**](https://gitlab.nsl.tuis.ac.jp/iseki/lticontainerspawner) は **Moodle(LMS)** から渡される **LTIカスタムパラメータ** を使用して，**JupyterHub** を制御する．
- さらに環境変数を使用して，コンテナ（Docker/Podman）を制御する．
- 追加参照 Wiki
  - [**mod_lticontainer**](https://gitlab.nsl.tuis.ac.jp/iseki/mod_lticontainer/-/wikis/mod_lticontainer-(J))
  - [**LTIContainerSpawner**](./LTIContainerSpawner)

\
![](uploads/4e7947c00da7af25c11cb90d2e53c86d/LTICTR.png){width="65%"}
- 注) Moodle 4.1.5 よりコアから **xmlrpc機能が削除**されたので，現在は ltictr_proxyから Moodleへの通信は **REST**を使用している．


# 機能
- 以下の機能をコース内の外部ツール（LTI設定）毎に設定可能．（同じJupyterHubホストに対して複数同時設定が可能）
  - 教師ユーザと学生ユーザの分離．(LTIContainerSpawner)
  - ユーザ毎の柔軟な Volume のマウント，及びアクセス権（教師と学生）の設定．(mod_lticontainer + LTIContainerSpawner)
    - マウントする Volume を自由に指定可能．
    - 教材の配布の自動化と収集用 Volume の設定．
  - ユーザの作業用スペースの設定．(mod_lticontainer + LTIContainerSpawner)
    - ユーザの作業用スペースへ任意の課題を自動コピー可能．
  - マウントした Volume への任意名でのアクセス．(mod_lticontainer + LTIContainerSpawner)
  - 起動コンテナイメージのリモート選択．(mod_lticontainer + LTIContainerSpawner)
  - 起動 URL（Lab/Notebook）の選択．(mod_lticontainer + LTIContainerSpawner)
  - コンテナの使用する CPU/Memory リソースの制限．(LTIContainerSpawner)
- ユーザグループのサポート．(JupyterHub + LTIContainerSpawner)
- JupyterHub ホスト側にユーザのホームディレクトリが無い場合は，自動生成する．(JupyterHub + LTIContainerSpawner)
- コンテナとして Podman を選択可能．(LTIContainerSpawner)
- iframe の一部サポート．(mod_lticontainer + LTIContainerSpawner)
  - 動くための条件がシビアなので（tornado のバージョンやWebブラウザの種類によって条件が変わる），"一部サポート" とする．
- 学生の学習状況のリアルタイムな可視化 (Ltictr_Proxy + Moodle)

# インストール と起動
## インストール
```plaintext
# git clone https://gitlab.nsl.tuis.ac.jp/iseki/lticontainerspawner.git
# cd lticontainerspawner
# make install
# vi /usr/local/etc/jupyterhub_lticontainer_config.py
     * 少なくとも以下の項目を変更する必要がある．
        my_ip_addr
        ltiauth_consumer_key
        ltiauth_secret_key
        ssl_server_cert
        ssl_private_key
        api_token
        time_zone
        time_format
```

## 起動
```plaintext
# vi /usr/lib/systemd/system/jupyterhub.service
# systemctl enable jupyterhub.service
# systemctl start  jupyterhub.service
```

# 設定
## 主な設定項
### jupyterhub_lticontainer_config.py
- **my_ip_addr**
  - ホストのIPアドレスを指定する．localhost (127.0.0.1) は使用できない
- **ltiauth_consumer_key, ltiauth_secret_key**
  - コンシューマ鍵と共有秘密鍵を設定する．
  - ２つの鍵は Moodle の外部ツールの鍵と合わせる．
  - 鍵自体は **openssl rand -hex 32** コマンドなどで生成する．
- **ssl_server_cert, ssl_private_key**
  - SSL/TLS 通信でのサーバ証明書と秘密鍵を指定する．(pem形式)
- **api_token**
  - **mod_lticotainer**（Moodle）の **JupyterHub API Token** に同じ値を設定する．
- **time_zone, time_format**
  - Time Zone と Timeフォーマットを指定する．（デフォルトは日本時）

### lticontainerspawner_config.py
- デフォルトで使用可能
- **projects_dir, works_dir**
  - ユーザのJupyter用ディレクトリを指定するための変数．
  - ユーザのJupyter用ディレクトリは **/home/\[グループ名\]/\[ユーザ名\]/\[projects_dir\]/\[works_dir\]/** となる．
- **spawner_environment**
  - コンテナに渡す環境変数を指定する．
  - ほぼ変更なしで大丈夫だが，**GRANT_SUDO** は状況に合わせて設定する．
    - **GRANT_SUDO** : 'yes' にするとコンテナ内で sudo コマンドが使用できる．

## LTI

### 処理可能な LTI カスタムパラメーター
- **lms_user**
- **lms_teacher**
- **lms_image**
- **lms_defurl**
- **lms_cpulimit** (デフォルトは 1)
- **lms_memlimit** (デフォルトは 500MiB)
- **lms_vol\_\[display name\]**
- **lms_sub\_\[display name\]**
- **lms_prs\_\[display name\]**
- 自動設定
  - lms_iframe
  - lms_sessioninfo
- 未使用
  - (lms_grname)
  - (lms_option)


* 各パラメータの詳細については [**mod_lticontainer**](https://gitlab.nsl.tuis.ac.jp/iseki/mod_lticontainer/-/wikis/mod_lticontainer-(J)) 参照

# コンテナイメージ

## start.sh

- 使用できるコンテナイメージとして，最低限イメージの **/usr/local/bin** に **bin/start.sh** がコピーされている必要がある．

#### Dockerfile 例

```plaintext
FROM jupyterhub/singleuser
USER root
ADD  bin/start.sh /usr/local/bin/
RUN  chmod a+rx /usr/local/bin/start.sh
```

## 使用可能なコンテナイメージ

一般の JupyterHub 用イメージも使用できるが，拡張機能は動作しない．

### GPU無し

- www.nsl.tuis.ac.jp:5000/jupyterhub-ltictr/base-notebook (Python)
- www.nsl.tuis.ac.jp:5000/jupyterhub-ltictr/singleuser (Python)
- www.nsl.tuis.ac.jp:5000/jupyterhub-ltictr/scipy-notebook (Python+SciPy)
- www.nsl.tuis.ac.jp:5000/jupyterhub-ltictr/tensorflow-notebook (Python+Tensorflow)
- www.nsl.tuis.ac.jp:5000/jupyterhub-ltictr/datascience-notebook (Python, Julia, R)
- www.nsl.tuis.ac.jp:5000/jupyterhub-ltictr/vhtec-notebook (Python, C++, JS, PHP)
- www.nsl.tuis.ac.jp:5000/jupyterhub-ltictr/xeus-cling (Python, C, C++)
- www.nsl.tuis.ac.jp:5000/jupyterhub-ltictr/java-notebook (Python, Java, JS)
- www.nsl.tuis.ac.jp:5000/jupyterhub-ltictr/php-notebookdokc (Python, PHP, JS)
- www.nsl.tuis.ac.jp:5000/jupyterhub-ltictr/swift-tensorflow (Python, Swift)
- www.nsl.tuis.ac.jp:5000/jupyterhub-ltictr/jupyterlab-broccoli (Blockly, XPython, JS, TS, Lua)

### GPU有

- www.nsl.tuis.ac.jp:5000/jupyterhub-ltictr/base-notebook-gpu (Python)
- www.nsl.tuis.ac.jp:5000/jupyterhub-ltictr/scipy-notebook-gpu (Python+SciPy)
- www.nsl.tuis.ac.jp:5000/jupyterhub-ltictr/tensorflow-notebook-gpu (Python+Tensorflow)

### ダウンロード例

```plaintext
# docker pull www.nsl.tuis.ac.jp:5000/jupyterhub-ltictr/singleuser   
```

# サポート機能
### ユーザ用
- **submit**
  - Submit Volume が一つしかない場合，カレントドライブにあるファイルを自動的にSubmit Volumeにコピーする（ディレクトリはコピーされない）．
  - コピーするファイルを限定したい場合は，ファイル名（複数）を直接指定する．
  - コピーする際にユーザIDがファイル名の先頭に付加される．
  - 使い方：`!submit`， `!submit kadai.ipynb`

### 教師用
- **extract**
  - Submit Volume が一つしかない場合，Submit Volume にある各ユーザのファイルから各設問ごとの採点用の ipynbファイルを生成する．
  - 使い方（コンソールから）：`!extract base_filename codenum [codenum] [codenum] ....`
  - 例：`!extract 1-1.ypynb 2 3 5`
- **tocsv**
  - 採点用の各設問ごとのipynbを各ユーザ毎にまとめたcsvファイルを生成する.
  - 使い方（コンソールから）：`!tocsv base_filename`
  - 例：`!tocsv 1-1.ipynb`