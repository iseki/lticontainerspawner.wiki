### We recommend using [Ltictr_Proxy](md/Ltictr_Proxy) rather than Feserver.

# Index
[[_TOC_]]

# Feserver (feplg_nbws.so module)
- subversion Repository: svn co http://www.nsl.tuis.ac.jp/svn/linux/feserver/trunk feserver
## Overview
- It is essentially a test tool with MITM-like behavior (TCP relay program).
    - It acts between the server and the client and relays the communication data.
- By loading various modules, it is possible to check the contents and perform various operations on the relayed communication data.
#### Example of modules
- feplg_nop.so module
    - A module that does nothing but relay communication data.
- feplg_nbws.so module
    - Module to analyze Jupyter's WebSocket communication.
## Functions
#### feplg_nbws.somodule
- A module that is at between Moodle and JupyterHub, collecting communication information (mainly WebSockt communication **data about users' progress**) and passing it to Moodle via XML-RPC.
- From the user's point of view, it looks like a reverse proxy.
- Features
    - Relay of communication data
    - Analysis of WebSocket communication data.
        - **Collecting data on users' learning progress**．
    - Analysis of HTTP(S) communications.
    - Inserting cookies.
    - **Passing the collected data to Moodle via XML-RPC communication**．(using the Moodle web service)
- Data visualization is under development.

## Install and Execution

### If you installed LTIContainerSpawner, already
```
# vi /usr/local/etc/nbws.conf
# vi /usr/lib/systemd/system/feserver.service
# systemctl enable feserver.service
# systemctl start  feserver.service
```

### Install and Execution by Manual
##### Manual Install
- Requires [JunkBox_Lib](https://www.nsl.tuis.ac.jp/xoops/modules/xpwiki/?JunkBox_Lib) to compile.
```
# svn co http://www.nsl.tuis.ac.jp/svn/linux/JunkBox_Lib/trunk JunkBox_Lib
# cd JunkBox_Lib
# ./config.sh
# ./configure --enable-ssl
# make
# cd ..
# svn co http://www.nsl.tuis.ac.jp/svn/linux/feserver/trunk feserver
# cd feserver
# make
# vi nbsw.conf
```

##### Manual Execution
```
# ./fesvr -h 172.22.1.75:8000 -p 9000 -m feplg_nbws.so -c -s --conf nbsw.conf
```
- Usage... ./fesvr -h host_name[:port] -p port -m module_path [-s] [-c] [-i] [-u user] [-f pid_file] [-d]  [--conf config_file]  [--cert cert_file] [--key key_file]
    - **-h** :  Server FQDN + port number. If the port number of the server is omitted, it is the same as the local port number. Required.
    - **-p** : Local port number. Required.
    - **-m** : Processing module. Required.
    - **-u** : Effective user name.
    - **-i** : Disables daemon mode. Specify this when using via a superdemon.
    - **-f** : pid file.
    - **-d** : Debugging mode. Verbose mode.
    - **-s** : It is an SSL/TLS connection to the server (fesrv is an SSL/TLS client). The server certificate is not verified.
    - **-c** : It is an SSL/TLS connection to the client (fesrv becomes an SSL/TLS server).
    - **--conf** : Configuration file specification. (**Common items have priority on the command line**)
    - **--cert** : The server certificate file when -c is specified. PEM format. It can also be specified in the configuration file.  Default is /etc/pki/tls/certs/server.pem
    - **--key**  : The server private secret key file when -c is specified. PEM format. It can also be specified in the configuration file. Default is /etc/pki/tls/private/key.pem

## Settings
### Configuration file for feplg_nbws.so (nbws.conf)
- ~~**Moodle_Host** =  Hostname (FQDN or IP address) for XML-RPC.~~ 
    - ~~Default is localhost~~
    - Change to automatic notification from LMS
- ~~**Moodle_Port** = Port number of the host for XML-RPC.~~
    - ~~Default is 80~~
    - Change to automatic notification from LMS
- ~~**Moodle_TLS** = Whether HTTPS should be used for XML-RPC.~~ 
    - ~~Default is FALSE.~~
    - Change to automatic notification from LMS
    - Note: When using HTTPS communication, the server certificate of the other (Moodle) host is not verified.
- ~~**Moodle_Token** = A token issued by Moodle.~~
    - ~~See below. (Required)~~
    - Change to automatic notification from LMS
- ~~Moodle_URL~~ **XmlRpc_Path** = Directory information of the URL where XML-RPC is performed. 
    - Default is **/webservice/xmlrpc/server.php**
- ~~Moodle_DBAns~~ **XmlRpc_Response** = Whether or not to show the response when XML-RPC.
    - For debugging. Default is **FALSE**.
- ~~Moodle_Service~~ **XmlRpc_Service** = The name of the XML-RPC service. 
    - There is no need to change it from the default. If you change it, it won't work. For debugging. 
    - Default is **mod_lticontainer_write_nbdata**.
- ~~Moodle_HTTP~~ **XmlRpc_HTTPver** = HTTP protocol version for XML-RPC. 1.1 or 1.0. 
    - Probably 1.1 is fine. The default is 1.1.

##### Common module settings
- **Fesvr_Server_Cert** = Setting for Feserver main body. server certificate for HTTPS communication (PEM format)
    - Default is /etc/pki/tls/certs/server.pem
- **Fesvr_Private_Key** = Setting for Feserver main body. Private Secret key file in case of HTTPS communication (PEM format)
    - Default is /etc/pki/tls/private/key.pem

