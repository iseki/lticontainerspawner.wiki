# Index

[[_TOC_]]

## 概要

- JupyterHub の [**configurable-http-proxy**](https://github.com/jupyterhub/configurable-http-proxy) の代用となるリバースプロキシサーバ
- [**configurable-http-proxy**](https://github.com/jupyterhub/configurable-http-proxy) との併用も可．
- [**mod_lticontainer**](https://gitlab.nsl.tuis.ac.jp/iseki/mod_lticontainer) で Charts機能を使用する場合に必要．

## 機能

- JupyterHub から通知を受けて動作する通信経路制御のための **ltictr_api_server** と，実際に通信を中継して WebSocketデータを解析する **ltictr_proxy_server** から成る．
- 通常は **ltictr_proxy_server** からチャイルドプロセスとして **ltictr_api_server** が起動される．
  - **ltictr_proxy_server** と **ltictr_api_server** はそれぞれ独立に起動することも可能．
- **ltictr_proxy_server** は単体で普通のWeb用リバースプロキシサーバとしても使用可能．
- 機能詳細
  - JupyterHub の通信経路制御に対応
  - 通信の中継
  - WebSocket 通信の解析．
    - **ユーザの学習状況に関するデータを収集**．
  - HTTP(S)通信の解析．
  - Cookie の挿入．（Moodle のコース識別を行うため）
  - **収集したデータを Moodle へ REST 通信で渡す**．（MoodleのWebサービスを使用）

## コマンド引数

#### ltictr_proxy_server

```plaintext
Usage... %s -p client_side_port [-c] [-h host_url[:port]] [-a [api_url:]port] [-u user] [-d] 
                      [--noexecapi] [--chunked] [--conf config_file] 
                      [--cert cert_file] [--key key_file] [--pid pid_file]
```

- **\-p** : Webブラウザの通信を受けるローカルサーバポート番号．必須．
- **\-c** : Webブラウザに対してSSL/TLS接続になる．
- **\-h** : WWWサーバを明示的に指定するためのURL．ltictr_api_server を使用する場合は省略可．
- **\-a** : ltictr_api_server を指定するためのURL．ポート番号のみの場合，URL部分は [http://127.0.0.1](http://127.0.0.1) となる．
- **\-u** : 実効ユーザ名．
- **\-d** : デバッグモード．
- **\-n**, **\--noexecapi** : ltictr_api_server を起動しない．
- **\--chunked** : chunkデータをchunkデータとして扱う．通常動作では chunkデータは，一旦全部受信されてから転送される．
- **\--conf** : 設定ファイルを指定する（**コマンドラインと競合する場合は，設定ファイル優先**）
- **\--cert** : -c を指定した場合のサーバ証明書：PEM形式．設定ファイル内でも指定可能．
  - デフォルトは /etc/pki/tls/certs/server.pem
- **\--key** : -c を指定した場合のサーバの秘密鍵：PEM形式．設定ファイル内でも指定可能．
  - デフォルトは /etc/pki/tls/private/key.pem
- **\--pid** : pid ファイルを指定する．
  - デフォルトは /var/run/ltictr_proxy.pid

#### ltictr_api_server

```plaintext
Usage... %s -a [api_url:]port [-u user] [-d] 
               [--conf config_file] [--cert cert_file] [--key key_file] [--apid pid_file] 
```

- **\-a** : サーバポートのURL．必須．FQDNの部分は無視する．TLS通信を行う場合は https://:8001 のように記述する．
- **\-u** : 実効ユーザ名．
- **\-d** : デバッグモード．
- **\--conf** : 設定ファイルを指定する（**コマンドラインと競合する場合は，設定ファイル優先**）
- **\--cert** : -c を指定した場合のサーバ証明書：PEM形式．設定ファイル内でも指定可能．
  - デフォルトは /etc/pki/tls/certs/server.pem
- **\--key** : -c を指定した場合のサーバの秘密鍵：PEM形式．設定ファイル内でも指定可能．
  - デフォルトは /etc/pki/tls/private/key.pem
- **\--pid** : pid ファイルを指定する．
  - デフォルトは /var/run/ltictr_proxy.pid

## 起動

#### systemd を使用する．

```plaintext
# vi /usr/local/etc/ltictr_proxy.conf
# vi /usr/lib/systemd/system/ltictr_proxy.service
# systemctl enable ltictr_proxy.service
# systemctl start  ltictr_proxy.service
```

#### 手動起動

##### 1\. 通常の起動

```plaintext
# ltictr_proxy_server -p 8100 -c -a 8001 --conf /usr/local/etc/ltictr_proxy.conf
# jupyterhub -f jupyterhub_XXX_config.py
```

##### 2\. ltictr_api_server と ltictr_proxy_server を別々に起動する場合

```plaintext
# ltictr_api_server -a 8001 --conf ./ltictr_proxy.conf
# ltictr_proxy_server -p 8100 -c -a 8001 -n --conf /usr/local/etc/ltictr_proxy.conf
# jupyterhub -f jupyterhub_XXX_config.py
```

##### 3\. configarable-http-proxy と併用する場合．または通常の Webリバースプロキシとして使用する場合

```plaintext
# ltictr_proxy_server -p 8100 -c -n -h http://202.26.150.55:8000 --conf /usr/local/etc/ltictr_proxy.conf
# jupyterhub -f jupyterhub_XXX_config.py
```

## 設定

### LTIContainerSpawner の設定

- 上記 **手動起動** で，**1, 2の場合**，JupyterHubで自動的に configurable-http-proxy が起動しないようにしなければならない．
- LTIContainerSpawner の設定ファイル（lticontainerspawner_config.py）

```plaintext
proxy_should_start  = False                     # ltictr_proxy を使用
#proxy_should_start  = True                      # configurable-http-proxy を使用
proxy_cleanup       = True
proxy_api_url       = 'http://localhost:8001'
proxy_api_token     = "ABCDEFG"                 # 下記 LTICTR_API_Token と同じにする
```

### 設定ファイル (/usr/local/etc/ltictr_proxy.conf）
- **Rest_Path** = RESTサービスプログラムへのパス.
    - 通常はデフォルトから変更する必要はない．
    - デフォルトは **/webservice/rest/server.php**
- **Rest_Service** = RESTサービス名 
    - 通常はデフォルトから変更する必要はない．
    - デフォルトは **mod_lticontainer_write_nblogs**
- **Rest_HTTPver** = HTTP のプロトコルバージョン．1.1 または 1.0 を指定．
    - デフォルトは 1.1
- **Rest_Response** = FALSE/TRUE． デバック用．Moodleからの XMLレスポンスを標準エラー出力に表示する．
  - デフォルトは **FALSE**
- **LTICTR_PID_File** = ltictr_proxy_server の pidファイル．
- **LTICTR_API_PID_File** = ltictr_api_server の pidファイル．
- **LTICTR_API_Token** = APIサーバの接続用トークン．任意の文字列．
  - **lticontainerspawner_config.py** の **proxy_api_token** と同じ値にする．
- **LTICTR_Server_Cert** = HTTPS通信を行う場合のサーバ証明書（PEM形式）
  - デフォルトは /etc/pki/tls/certs/server.pem
- **LTICTR_Private_Key** = HTTPS通信を行う場合の秘密鍵ファイル（PEM形式）
  - デフォルトは /etc/pki/tls/private/key.pem